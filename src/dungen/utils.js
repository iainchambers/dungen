export default {
	Number(x)
	{
		this.clamp = (min, max) =>
		{
			return Math.min(Math.max(x, min), max);
		};

		this.toNearest = (round) =>
		{
			return Math.floor(x / round) * round;
		};

		this.between = (min, max, allowEqual = true) => 
		{
			if(allowEqual) return x >= Math.min(min, max) && x <= Math.max(min, max);
			return x > Math.min(min, max) && x < Math.max(min, max);
		};

		return this;
	},
	Event(ev)
	{
		this.getMousePosition = (rect) =>
		{
			return {
				x: ev.clientX - rect.left - document.documentElement.scrollLeft,
				y: ev.clientY - rect.top - document.documentElement.scrollTop,
			};
		}

		return this;
	},
	Coords(x, y)
	{
		this.toBlock = (gridSize) => 
		{
			return { x: x - (x % gridSize), y: y - (y % gridSize), width: gridSize, height: gridSize }
		};

		return this;
	}

}

