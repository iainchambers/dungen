import React, { Component } from 'react'

import Blocks from './components/blocks'

import './dungen.css';

class Dungen extends Component {

	constructor(props)
	{
		super(props);

		this.state = {
			canvas: {},
		};
	}

	componentDidMount()
	{
		const canvas = this.svg.getBoundingClientRect();

		this.setState({ canvas });
	}

	render()
	{
		const gridSize = 20;
		const { canvas } = this.state;

		return (

			<main className="dungen">
				<svg ref={elem => this.svg = elem}>
					<defs>
						<filter id="drop-shadow" height="130%" width="130%">
							<feGaussianBlur in="SourceAlpha" stdDeviation="3" />
							<feOffset dx="0" dy="0" result="offsetblur" />
							<feMerge> 
								<feMergeNode /> 
								<feMergeNode in="SourceGraphic" />
							</feMerge>
						</filter>
						<pattern id="grid" width={gridSize} height={gridSize} patternUnits="userSpaceOnUse">
					      <rect width={gridSize} height={gridSize} fill="#333333" stroke="#454545" />
					    </pattern>
					</defs>
					<Blocks canvas={canvas} gridSize={gridSize} />
				</svg>
			</main>
		);
	}
}


export default Dungen;