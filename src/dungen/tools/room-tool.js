import React, { Component } from 'react'

import Utils from '../utils'

export default class extends Component {

	constructor(props)
	{
		super(props);

		this.state = {
			origin: null,
			outline: null,
		};
	}

	mouseDown(ev)
	{
		const { x, y } = Utils.Event(ev).getMousePosition(this.props.canvas);

		let origin = {
			x: Utils.Number(x).toNearest(this.props.gridSize),
			y: Utils.Number(y).toNearest(this.props.gridSize),
		};

		this.setState({ origin });
	}

	mouseMove(ev)
	{
		const { origin } = this.state;

		if(!origin) return;

		let { x, y } = Utils.Event(ev).getMousePosition(this.props.canvas);

		x = Utils.Number(x).toNearest(this.props.gridSize);
		y = Utils.Number(y).toNearest(this.props.gridSize);

		const outline = {
			x: Math.min(x, origin.x),
			y: Math.min(y, origin.y),
			width: Math.abs(x - origin.x),
			height: Math.abs(y - origin.y),
			fill: '#FFFFFF',
			stroke: '#666666',
			opacity: .5,
		};

		this.setState({ outline });
	}

	mouseUp(ev)
	{
		const { outline } = this.state;

		const { rooms } = this.props;

		if(outline)	this.props.addRoom(Object.assign(outline, { label: `Room ${rooms.length + 1}` }));

		this.setState({ outline: null, origin: null });
	}

	render()
	{	
		const { outline } = this.state;

		return <g className="room-tool" onMouseDown={this.mouseDown.bind(this)} onMouseMove={this.mouseMove.bind(this)} onMouseUp={this.mouseUp.bind(this)}>
			<rect width="100%" height="100%" fill="transparent" />
			{outline ? <rect {...outline}></rect> : null}
		</g>;
	}
}