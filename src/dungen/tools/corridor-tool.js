import React, { Component } from 'react'

import Utils from '../utils'

export default class extends Component {

	constructor(props)
	{
		super(props);

		this.state = {
			origin: null,
			destination: null,
			outline: null
		};
	}

	grabHandle(handle, ev)
	{
		this.dragHandle = handle;
	}

	releaseHandle()
	{
		this.dragHandle = null;
	}

	mouseDown(ev)
	{
		const origin = Utils.Event(ev).getMousePosition(this.props.canvas);

		origin.x = origin.x - (origin.x % this.props.gridSize) + (this.props.gridSize / 2)
		origin.y = origin.y - (origin.y % this.props.gridSize) + (this.props.gridSize / 2)

		this.setState({ origin })
	}

	mouseMove(ev)
	{
		const { origin } = this.state;

		if(!origin) return;

		const destination = Utils.Event(ev).getMousePosition(this.props.canvas);

		destination.x = destination.x - (destination.x % this.props.gridSize) + (this.props.gridSize / 2)
		destination.y = destination.y - (destination.y % this.props.gridSize) + (this.props.gridSize / 2)

		this.setState({ destination });
	}

	mouseUp(ev)
	{
		this.props.addCorridor([[this.state.origin.x, this.state.origin.y], [this.state.destination.x, this.state.destination.y]]);

		this.setState({ origin: null, destination: null });
	}


	render()
	{	
		const { rooms, canvas, gridSize } = this.props;
		
		let { origin, destination } = this.state;

		let preview;
		
		if(origin && destination)
		{
			if(Math.abs(destination.x - origin.x) >= Math.abs(destination.y - origin.y)) destination.y = origin.y;
			else destination.x = origin.x;

			preview = <g onMouseUp={this.mouseUp.bind(this)}>
				<path fill="none" stroke="#CCCCCC" strokeDasharray="3,2" d={`M${origin.x} ${origin.y} L${destination.x} ${destination.y}`}></path>
				<circle r="2" cx={origin.x} cy={origin.y} fill="#CCCCCC"></circle>
				<circle r="2" cx={destination.x} cy={destination.y} fill="#CCCCCC"></circle>
			</g>;
		}

		return <g className="corridor-tool">
			<rect width="100%" height="100%" fill="transparent" ref={canvas => this.canvas = canvas} onMouseDown={this.mouseDown.bind(this)} onMouseMove={this.mouseMove.bind(this)} onMouseUp={this.mouseUp.bind(this)} />
			{ preview }
		</g>;
	}
}