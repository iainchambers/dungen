import React, { Component } from 'react'

import Utils from '../utils'

class Block extends Component 
{
	render()
	{
		const { x, y, out, gridSize, fill } = this.props;

		return <rect x={x} y={y} width={gridSize} height={gridSize} className={`block ${out ? 'out' : null}`} fill={'#FFFFFF'} />;
	}
}

class Label extends Component
{
	render()
	{
		const { x, y, width, height, gridSize, text } = this.props;

		return <g>
			<rect x={x} y={y} width={width} height={height} fill="lightblue" stroke="steelblue" />
			<text textAnchor="middle" x={x + width / 2} y={y + height / 2} alignmentBaseline="middle">{this.props.children}</text>
		</g>;
	}
}

class Blocks extends Component 
{
	constructor(props)
	{
		super(props);

		this.gridSize = 20;

		this.state = {
			blocks: new Map(),
			highlight: {
				x: 0,
				y: 0,
				width: this.gridSize,
				height: this.gridSize,
				fill: '#666666',
			},
		};


	}

	mouseMove(ev)
	{	
		let { x, y } = Utils.Event(ev).getMousePosition(this.props.canvas);
		
		let { origin, highlight } = this.state;

		if(!origin)
		{
			highlight.x = x - (x % this.gridSize);
			highlight.y = y - (y % this.gridSize);
			highlight.width = this.gridSize;
			highlight.height = this.gridSize;
			highlight.fill = '#666666';

			this.setState({ highlight });

			return;
		}

		x = x - (x % this.gridSize);
		y = y - (y % this.gridSize);

		let width = Math.abs(x - origin.x);
		let height = Math.abs(y - origin.y);

		highlight.x = Math.min(origin.x, x);
		highlight.y = Math.min(origin.y, y);

		width = width - (width % this.gridSize) + this.gridSize;
		height = height - (height % this.gridSize) + this.gridSize;

		if(ev.shiftKey)
		{
			highlight.width = width;
			highlight.height = height;
		}
		else if(height > width)
		{
			highlight.x = origin.x;
			highlight.width = this.gridSize;
			highlight.height = height;
		}
		else
		{
			highlight.y = origin.y;
			highlight.width = width;
			highlight.height = this.gridSize;
		}

		highlight.fill = origin.func === '-' ? 'red' : '#666666';

		this.setState({ highlight });
	}

	mouseDown(ev)
	{
		const { x, y } = Utils.Event(ev).getMousePosition(this.props.canvas);

		const origin = {
			x: x - x % this.gridSize,
			y: y - y % this.gridSize,
			func: ev.button > 0 ? '-' : '+',
		};
		
		this.setState({ origin });
	}

	mouseUp(ev)
	{
		ev.preventDefault();

		let { highlight, blocks } = this.state;

		blocks = new Map(blocks);

		let x, y;

		for(let row = 0; row < highlight.height / this.gridSize; row++)
		{
			for(let col = 0; col < highlight.width / this.gridSize; col++)
			{
				x = highlight.x + (col * this.gridSize);
				y = highlight.y + (row * this.gridSize);

				if(ev.button > 0)
				{
					if(blocks.has(`${x},${y}`)) 
					{
						blocks.set(`${x},${y}`, Object.assign(blocks.get(`${x},${y}`), { out: true }));

						((x, y) => {
							window.setTimeout(() => blocks.delete(`${x},${y}`), 250);
						})(x, y);
					}
				}
				else if(!blocks.has(`${x},${y}`)) 
				{
					blocks.set(`${x},${y}`, { x: highlight.x + (col * this.gridSize), y: highlight.y + (row * this.gridSize) });
				}
			}
		}

		

		this.setState({ origin: null, blocks });
	}

	findRooms(blocks)
	{


		const rooms = [];

		return rooms;
	}

	render()
	{
		const { blocks, highlight } = this.state;

		const rooms = this.findRooms(blocks);
		

		return <g className="blocks">
			<rect width="100%" height="100%" fill="url(#grid)"></rect>
			{ [...blocks.values()].map((block, index) => <Block key={`${block.x},${block.y}`} {...block} gridSize={this.gridSize} />) }

			{ rooms.map((room, index) => <Label key={index} {...room}>{`Room ${index + 1}`}</Label>) }
			{ 
				highlight ?
					<g>
						<rect 
					 		{...highlight}
					 		onMouseMove={this.mouseMove.bind(this)}
					 		onMouseUp={ev => this.stopDragging()}
					 		opacity=".5"
					 	/>
					 	<path id="highlight-path" d={ 
			 				[0, 1, 2, 3].map(line => 
			 				{
			 					switch(true)
			 					{
			 						case line === 0:
			 							return `M${highlight.x + 10} ${highlight.y + 10}`;
			 						case line === 1 && highlight.width > this.gridSize:
			 							return `H${highlight.x + highlight.width - 10}`;
			 						case line === 2 && highlight.height > this.gridSize:
			 							return `V${highlight.y + highlight.height - 10}`;
			 						case line === 3 && highlight.width > this.gridSize && highlight.height > this.gridSize:
			 							return `H${highlight.x + 10} Z`;
			 						default:
			 							return null;
			 					}
			 				}).join(' ')
			 			} stroke="#CCCCCC" fill="none" strokeDasharray="3,2" /> : null
			 			<g>
			 				<circle r="3" fill="#CCCCCC" cx={highlight.x + 10} cy={highlight.y + 10}></circle>
			 				<circle r="3" fill="#CCCCCC" cx={highlight.x + highlight.width - 10} cy={highlight.y + 10}></circle>
			 				<circle r="3" fill="#CCCCCC" cx={highlight.x + highlight.width - 10} cy={highlight.y + highlight.height - 10}></circle>
			 				<circle r="3" fill="#CCCCCC" cx={highlight.x + 10} cy={highlight.y + highlight.height - 10}></circle>
			 			</g>
					</g> : null
			}
			<rect width="100%" height="100%" fill="transparent" onMouseDown={this.mouseDown.bind(this)} onMouseMove={this.mouseMove.bind(this)} onMouseUp={this.mouseUp.bind(this)} onContextMenu={ev => ev.preventDefault()}></rect>
		</g>;
	}
}

export default Blocks