import React, { Component } from 'react';

import Dungen from './dungen';

import './App.css';



class App extends Component 
{
	render() 
	{

		return (
			<div className="App" ref={elem => this.container = elem}>
	 			<Dungen />
			</div>
		);
	}
}

export default App;